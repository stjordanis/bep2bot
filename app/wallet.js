/**
 * Local Imports
 */
const config = require('../config/config').config;
const utils = require('./utils');
const binance = require('./binance')
const users = require('./users');
const mongo = require('./mongo');

/**
 * Handles the withdrawal request from a user
 * @param  {Object} messageObj   Telegram message Object
 * @return {String}              String to return to the user via msg
 */
const withdrawTips = (messageObj, callback) => {
    let withdrawObj = utils.getWithdrawObj(messageObj);

    if(!withdrawObj || withdrawObj.message){
        let outMsg = utils.format(config.botStrings.error_withdraw,utils.sexyError(withdrawObj.message))
        return callback(outMsg);
    } else {

    validateWithdraw(withdrawObj)
        .then(result => {
            processWithdrawal(utils.grabUser(withdrawObj.from), withdrawObj.amount, utils.tickerToToken(withdrawObj.token), function(error,result){
                let outMsg = error ? utils.format(config.botStrings.error_withdraw,utils.sexyError(error.message)) : result
                return callback(outMsg)
            })
        })
        .catch(error => {
            let outMsg = utils.format(config.botStrings.error_withdraw,utils.sexyError(error.message))
            return callback(outMsg)
        })
    }
}

/**
 * Validates a tip withdrawal request
 * @param  {Object} withdrawRequest     An object containing the users info & command arguments
 * @return {Object} validationArray     An array of validation promises
 */

const validateWithdraw = async(withdrawRequest) => {
    let isUser = utils.isUser(withdrawRequest.from)
    let isNumber = utils.isNumber(withdrawRequest.amount)
    let isValidToken = utils.isValidToken(withdrawRequest.token)
    let hasBalance = utils.hasBalance(utils.grabUser(withdrawRequest.from), utils.tickerToToken(withdrawRequest.token), withdrawRequest.amount)
    let hasAddress = utils.hasAddress(utils.grabUser(withdrawRequest.from))
    let validationArray = await Promise.all([isUser, isNumber, isValidToken, hasBalance, hasAddress])
    return validationArray
}


/**
 * Processess a withdrawal after validation has been completed
 * @param  {Object} userData        The bots user object
 * @param  {Number} amount          The amount of tokens to withdraw
 * @param  {String} token           The BEP2 token to withdraw eg. RUNE-B1A
 * @return {String}                 String to return to the user via msg
 */
const processWithdrawal = (userData, amount, token, callback) =>{
    binance.transferToken(userData.userid, userData.bepaddress, amount, token)
        .then(txHash => {
                let debitAmt = Math.abs(amount) * -1;

                Promise.all([
                    users.updateBalance(userData, debitAmt, token),
                    updateLedger(userData, debitAmt, token, txHash)
                ])
                    .then(res => {
                        let newBalance = res[0].balances.filter(x => x.token == token).map(x => x.balance)[0]
                        let outMsg = utils.format(config.botStrings.success_withdrawal,amount,token.toUpperCase(), newBalance, txHash)
                        return callback(null, outMsg)
                    })
                    .catch(error => {
                        utils.winston.warn(error)
                        return callback(error)
                    })
        .catch(error => {
            utils.winston.warn(error)
            return callback(error)
        })
    })
}


/**
 * Returns the ledger data for a user
 * @param  {Object} userObj      The telegram object
 * @return {String}              Message String
 */
function getTransactions(messageObj, callback){
    return new Promise((resolve, reject) => {
        let userInfo = utils.grabUser(messageObj.from)

        if(!userInfo){
            let error = new Error("USER_NOTREG")
            reject(error)
        }

        let rawData = config.botLedger.filter(x => x.userid == userInfo.userid);
        let userTransactions = utils.multisort(rawData,['date'], ['DESC']);
        if(!userTransactions || userTransactions.length == 0) {
            let error = new Error("TIP_NOTRANSACTIONS")
            reject(error)
        } else {
            let userTxData =  userTransactions.map(x => utils.formatLedger(x))
            resolve(userTxData);
        }
    })
}



/**
 * Updates the transaction ledger
 * @param  {Object} userObj     The telegram user object
 * @param  {Number} amt         The amount of the transaction
 * @param  {String} token       The token for the transaction
 * @param  {String} hash        The hash of the transaction
 * @return {Object}             An object containing the ledger for the user
 */
function updateLedger(userObj, amt, token, hash){
    return new Promise((resolve, reject) => {
        mongo.dbUpdateLedger(userObj, amt, token, hash)
        .then(ledgerEntry => {
            config.botLedger.push(ledgerEntry)
            resolve(ledgerEntry)
        })
        .catch(err => {
            console.log(err)
            let error = new Error("DB_FAIL")
            reject(error);
        })
    })
}


/**
 * EXPORTS
 */
exports.withdrawTips = withdrawTips;
exports.getTransactions = getTransactions;
exports.updateLedger = updateLedger;
